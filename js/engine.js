$(function() {

// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.main-menu').slideToggle(200);
});


if ($(window).width() <= '768'){
	$('.main-menu a').click(function() {
		$(this).parents('.main-menu').fadeToggle();
		$(".burger-menu").toggleClass('active')
	})
}




// Фиксированное меню при прокрутке страницы вниз

$(window).scroll(function(){
    var sticky = $('.header-top'),
        scroll = $(window).scrollTop();
    if (scroll > 200) {
        sticky.addClass('header-top__fixed');
    } else {
        sticky.removeClass('header-top__fixed');
    };
});




$(".main-menu").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - 90}, 1500);
  });




})